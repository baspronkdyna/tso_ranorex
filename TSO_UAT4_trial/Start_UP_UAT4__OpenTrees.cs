﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace TSO_UAT4
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The Start_UP_UAT4__OpenTrees recording.
    /// </summary>
    [TestModule("454b1436-cc3e-4baa-a5b3-a378117aa979", ModuleType.Recording, 1)]
    public partial class Start_UP_UAT4__OpenTrees : ITestModule
    {
        /// <summary>
        /// Holds an instance of the TSO_UAT4Repository repository.
        /// </summary>
        public static TSO_UAT4Repository repo = TSO_UAT4Repository.Instance;

        static Start_UP_UAT4__OpenTrees instance = new Start_UP_UAT4__OpenTrees();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Start_UP_UAT4__OpenTrees()
        {
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static Start_UP_UAT4__OpenTrees Instance
        {
            get { return instance; }
        }

#region Variables

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", "5.4.4")]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", "5.4.4")]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.0;

            Init();

            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left DoubleClick item 'MainForm.DataPanel.Technical_Service_Order' at Center.", repo.MainForm.DataPanel.Technical_Service_OrderInfo, new RecordItemIndex(0));
            repo.MainForm.DataPanel.Technical_Service_Order.DoubleClick();
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left DoubleClick item 'MainForm.DataPanel.Maintenance' at Center.", repo.MainForm.DataPanel.MaintenanceInfo, new RecordItemIndex(1));
            repo.MainForm.DataPanel.Maintenance.DoubleClick();
            Delay.Milliseconds(200);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
