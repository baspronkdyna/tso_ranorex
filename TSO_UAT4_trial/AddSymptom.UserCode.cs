﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// Your custom recording code should go in this file.
// The designer will only add methods to this file, so your custom code won't be overwritten.
// http://www.ranorex.com
// 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;

namespace TSO_UAT4
{
    public partial class AddSymptom
    {
        /// <summary>
        /// This method gets called right after the recording has been started.
        /// It can be used to execute recording specific initialization code.
        /// </summary>
        private void Init()
        {
            // Your recording specific initialization code goes here.
        }

        // Random number of Key sequence '{Down}
        public void Random_Press_Down(int Min, int Max)
        {
        	Random rnd = new Random();
			int count = rnd.Next(Min, Max + 1); // creates a number between Min and Max
			Report.Log(ReportLevel.Info, "Info", "Random press down key is " + count.ToString() + ".");
			for (int i = 0;i< count ;i++ )
			{
				Keyboard.Press("{Down}");				
			}
			Keyboard.Press("{Enter}");
			Report.Log(ReportLevel.Info, "Keyboard", "Key sequence '{Down}'.");
        }

        public void Key_Sequence_TextBoxMaskBox()
        {
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence from variable '$VarCustomerReference' with focus on 'MainForm.GrpSymptoms.TextBoxMaskBox'.", repo.MainForm.GrpSymptoms.TextBoxMaskBoxInfo);
            
            if (VarCustomerReference == "RanorexDefault01")
            {
            	Report.Log(ReportLevel.Error, "Error", "Unabale to retrieve customer reference from DB!");
            }
            else
            {
            	String newValue = VarCustomerReference.Remove(0, 7);
	            int tempNr = Convert.ToInt32(newValue);
	            tempNr++;
	            Report.Log(ReportLevel.Success, "Success", "Het volgende nummer wordt " + "Ranorex" + tempNr.ToString());
	            
	            repo.MainForm.XtraTabPage1.Customer_RMANumber.PressKeys("Ranorex" + tempNr.ToString());
            }
        }

      }
}