﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace TSO_UAT4
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The ProofOfPurchase recording.
    /// </summary>
    [TestModule("0e12ff8b-07c8-4a5f-87ea-ead18b3755d7", ModuleType.Recording, 1)]
    public partial class ProofOfPurchase : ITestModule
    {
        /// <summary>
        /// Holds an instance of the TSO_UAT4Repository repository.
        /// </summary>
        public static TSO_UAT4Repository repo = TSO_UAT4Repository.Instance;

        static ProofOfPurchase instance = new ProofOfPurchase();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public ProofOfPurchase()
        {
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static ProofOfPurchase Instance
        {
            get { return instance; }
        }

#region Variables

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", "5.4.4")]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", "5.4.4")]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.0;

            Init();

            Report.Log(ReportLevel.Info, "Delay", "Waiting for 1000ms.", new RecordItemIndex(0));
            Delay.Duration(1000, false);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MainForm.XtraTabPage1.IPhysicallyIncludeACopyOfTheDocum' at 10;9.", repo.MainForm.XtraTabPage1.IPhysicallyIncludeACopyOfTheDocumInfo, new RecordItemIndex(1));
            repo.MainForm.XtraTabPage1.IPhysicallyIncludeACopyOfTheDocum.Click("10;9");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MainForm.Next' at 77;11.", repo.MainForm.NextInfo, new RecordItemIndex(2));
            repo.MainForm.Next.Click("77;11");
            Delay.Milliseconds(200);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
