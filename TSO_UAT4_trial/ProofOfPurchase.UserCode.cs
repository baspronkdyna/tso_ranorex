﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// Your custom recording code should go in this file.
// The designer will only add methods to this file, so your custom code won't be overwritten.
// http://www.ranorex.com
// 
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;

namespace TSO_UAT4
{
    public partial class ProofOfPurchase
    {
        /// <summary>
        /// This method gets called right after the recording has been started.
        /// It can be used to execute recording specific initialization code.
        /// </summary>
        private void Init()
        {
            // Your recording specific initialization code goes here.
        }

        public int SelectRandomSymptGrp(int AantalSymptGrps)
        {
        	Random rnd = new Random();
        	return rnd.Next(0, AantalSymptGrps - 1);
        }

        public string getImei()
        {
        	System.DateTime date = System.DateTime.Now;
        	String result = date.ToString("MMddyyyyHHmmssf");
        	return result;
        }

        public void Key_Sequence_TextBoxMaskBox()
        {
        	string VarIMEI=this.getImei();
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence '" + VarIMEI + "' with focus on 'MainForm.XtraTabPage1.TextBoxMaskBox'.", repo.MainForm.XtraTabPage1.TextBoxSerial_IMEIInfo);
            repo.MainForm.XtraTabPage1.TextBoxSerial_IMEI.PressKeys(VarIMEI);
        }

        public void Mouse_Click_StaticText()
        {
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MainForm.GrpSymptoms.StaticText' at 162;7.", repo.MainForm.GrpSymptoms.StaticTextInfo);
            repo.MainForm.GrpSymptoms.StaticText.Click("162;7");
        }

        public void Mouse_Click_Buttons()
        {
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'DynafixUserPortalLauncherNet40.Buttons' at 31;6.", repo.DynafixUserPortalLauncherNet40.ButtonsInfo);
            repo.DynafixUserPortalLauncherNet40.Buttons.Click("31;6");
        }


        public void Random_Press_Down(int Min, int Max)
        {
        	Random rnd = new Random();
			int count = rnd.Next(Min, Max + 1); // creates a number between Min and Max
			Report.Log(ReportLevel.Info, "Info", "Random press down key is " + count.ToString() + ".");
			for (int i = 0;i< count ;i++ )
			{
				Keyboard.Press("{Down}");				
			}
			Keyboard.Press("{Enter}");
			Report.Log(ReportLevel.Info, "Keyboard", "Key sequence '{Down}'.");
        }

//        public void Mouse_Click_Open()
//        {
//           //	/form[@controlname='MainForm']/?/?/form[@controlname='RetrieveSymptomForm']/container[@controlname='grpSymptoms']/?/?/element[@controlname='cmbSymptomGroup']
//        	
//        	//Ranorex.ComboBox = repo.MainForm.SelfInfo.c
//        	//repo.DynafixUserPortalLauncherNet40.
//        	//repo.MainForm.GrpSymptoms. GrpSymptoms
//        	ComboBox cb = repo.MainForm.GrpSymptoms.ComboBox.; //(ComboBox)repo.MainForm.GrpSymptoms.StaticText;
//                    
//            
//            Random rnd = new Random();
//            cb.SelectedItem = cb.Items[rnd.Next(0, cb.Items.Count() - 1)];
//        }

    }
}