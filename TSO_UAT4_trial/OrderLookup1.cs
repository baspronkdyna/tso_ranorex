﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace TSO_UAT4
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The OrderLookup1 recording.
    /// </summary>
    [TestModule("0ff47f3e-5a1b-4e8e-85c3-3a5e7c51dd2d", ModuleType.Recording, 1)]
    public partial class OrderLookup1 : ITestModule
    {
        /// <summary>
        /// Holds an instance of the TSO_UAT4Repository repository.
        /// </summary>
        public static TSO_UAT4Repository repo = TSO_UAT4Repository.Instance;

        static OrderLookup1 instance = new OrderLookup1();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public OrderLookup1()
        {
            VarCustomerReference_new = "RanorexDefault01";
            VarTSNumber_new = "TSDefault01";
            VarServicePolicy = "Belgacom - Repair and return";
            VarPolicyAfterEntry = "Repair and Return : Default";
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static OrderLookup1 Instance
        {
            get { return instance; }
        }

#region Variables

        string _VarCustomerReference_new;

        /// <summary>
        /// Gets or sets the value of variable VarCustomerReference_new.
        /// </summary>
        [TestVariable("06860334-f957-4009-b7f0-f783e7e13c19")]
        public string VarCustomerReference_new
        {
            get { return _VarCustomerReference_new; }
            set { _VarCustomerReference_new = value; }
        }

        string _VarPolicyAfterEntry;

        /// <summary>
        /// Gets or sets the value of variable VarPolicyAfterEntry.
        /// </summary>
        [TestVariable("1d754a8e-1101-4015-9720-30cc7710104c")]
        public string VarPolicyAfterEntry
        {
            get { return _VarPolicyAfterEntry; }
            set { _VarPolicyAfterEntry = value; }
        }

        /// <summary>
        /// Gets or sets the value of variable VarTSNumber_new.
        /// </summary>
        [TestVariable("fc683ffe-1cd6-4bf5-934b-1b5eef7941a2")]
        public string VarTSNumber_new
        {
            get { return repo.VarTSNumber_new; }
            set { repo.VarTSNumber_new = value; }
        }

        /// <summary>
        /// Gets or sets the value of variable VarServicePolicy.
        /// </summary>
        [TestVariable("2ccdad8c-6bfb-4a5a-97d6-ebacfdcab304")]
        public string VarServicePolicy
        {
            get { return repo.VarServicePolicy; }
            set { repo.VarServicePolicy = value; }
        }

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", "5.4.4")]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", "5.4.4")]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.0;

            Init();

            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left DoubleClick item 'MainForm.DataPanel.ColMenu_OrderLookup' at 40;9.", repo.MainForm.DataPanel.ColMenu_OrderLookupInfo, new RecordItemIndex(0));
            repo.MainForm.DataPanel.ColMenu_OrderLookup.DoubleClick("40;9");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MainForm.SearchFormOrderNumber' at 66;6.", repo.MainForm.SearchFormOrderNumberInfo, new RecordItemIndex(1));
            repo.MainForm.SearchFormOrderNumber.Click("66;6");
            Delay.Milliseconds(200);
            
            Key_Sequence_TextBoxMaskBox1();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MainForm.Search' at 67;17.", repo.MainForm.SearchInfo, new RecordItemIndex(3));
            repo.MainForm.Search.Click("67;17");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left DoubleClick item 'MainForm.TSONumberRow0' at 45;9.", repo.MainForm.TSONumberRow0Info, new RecordItemIndex(4));
            repo.MainForm.TSONumberRow0.DoubleClick("45;9");
            Delay.Milliseconds(200);
            
            // Validate Service
            Report.Log(ReportLevel.Info, "Validation", "Validate Service\r\nValidating AttributeEqual (MaskBoxText=$VarServicePolicy) on item 'MainForm.GrpGeneral.lblServiceValue'.", repo.MainForm.GrpGeneral.lblServiceValueInfo, new RecordItemIndex(5));
            Validate.Attribute(repo.MainForm.GrpGeneral.lblServiceValueInfo, "MaskBoxText", VarServicePolicy);
            Delay.Milliseconds(100);
            
            // Validate Policy
            Report.Log(ReportLevel.Info, "Validation", "Validate Policy\r\nValidating AttributeEqual (MaskBoxText=$VarPolicyAfterEntry) on item 'MainForm.GrpGeneral.lblPolicyAfterOrderEntryValue'.", repo.MainForm.GrpGeneral.lblPolicyAfterOrderEntryValueInfo, new RecordItemIndex(6));
            Validate.Attribute(repo.MainForm.GrpGeneral.lblPolicyAfterOrderEntryValueInfo, "MaskBoxText", VarPolicyAfterEntry);
            Delay.Milliseconds(100);
            
            // Get the TSNumber
            GetTSNumber();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MainForm.Close2' at 14;8.", repo.MainForm.Close2Info, new RecordItemIndex(8));
            repo.MainForm.Close2.Click("14;8");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MainForm.Close2' at 15;8.", repo.MainForm.Close2Info, new RecordItemIndex(9));
            repo.MainForm.Close2.Click("15;8");
            Delay.Milliseconds(200);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
