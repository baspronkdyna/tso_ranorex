﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace TSO_UAT4
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The StartUserPortal_UAT4 recording.
    /// </summary>
    [TestModule("b1002a0a-f34d-43ec-9e39-d821bdd1359e", ModuleType.Recording, 1)]
    public partial class StartUserPortal_UAT4 : ITestModule
    {
        /// <summary>
        /// Holds an instance of the TSO_UAT4Repository repository.
        /// </summary>
        public static TSO_UAT4Repository repo = TSO_UAT4Repository.Instance;

        static StartUserPortal_UAT4 instance = new StartUserPortal_UAT4();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public StartUserPortal_UAT4()
        {
            VarUAT4PortalPath = "C:\\Dynafix User Portal UAT 4\\Dynafix.UserPortal.Launcher.Net4.0.exe";
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static StartUserPortal_UAT4 Instance
        {
            get { return instance; }
        }

#region Variables

        string _VarUAT4PortalPath;

        /// <summary>
        /// Gets or sets the value of variable VarUAT4PortalPath.
        /// </summary>
        [TestVariable("5f0d5e54-709e-447e-8d7c-b5cf65f7dff5")]
        public string VarUAT4PortalPath
        {
            get { return _VarUAT4PortalPath; }
            set { _VarUAT4PortalPath = value; }
        }

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", "5.4.4")]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", "5.4.4")]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.0;

            Init();

            Report.Log(ReportLevel.Info, "Application", "Run application with file name from variable $VarUAT4PortalPath with arguments '' in normal mode.", new RecordItemIndex(0));
            Host.Local.RunApplication(VarUAT4PortalPath, "", "C:\\Dynafix User Portal UAT 4", false);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MainForm.Navigation' at Center.", repo.MainForm.NavigationInfo, new RecordItemIndex(1));
            repo.MainForm.Navigation.Click();
            Delay.Milliseconds(200);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
