﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace TSO_UAT4
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The ServiceResult recording.
    /// </summary>
    [TestModule("4aa4b7b0-2ee1-4d0d-bd64-f868621a01e3", ModuleType.Recording, 1)]
    public partial class ServiceResult : ITestModule
    {
        /// <summary>
        /// Holds an instance of the TSO_UAT4Repository repository.
        /// </summary>
        public static TSO_UAT4Repository repo = TSO_UAT4Repository.Instance;

        static ServiceResult instance = new ServiceResult();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public ServiceResult()
        {
            VarTSNumber_new = "";
            ServiceResultCode = "34";
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static ServiceResult Instance
        {
            get { return instance; }
        }

#region Variables

        string _ServiceResultCode;

        /// <summary>
        /// Gets or sets the value of variable ServiceResultCode.
        /// </summary>
        [TestVariable("b4423dca-8f76-4679-b1ee-e6159a592de8")]
        public string ServiceResultCode
        {
            get { return _ServiceResultCode; }
            set { _ServiceResultCode = value; }
        }

        /// <summary>
        /// Gets or sets the value of variable VarTSNumber_new.
        /// </summary>
        [TestVariable("f4d23dcf-7d25-4d06-94fd-f39dc85eba05")]
        public string VarTSNumber_new
        {
            get { return repo.VarTSNumber_new; }
            set { repo.VarTSNumber_new = value; }
        }

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", "5.4.4")]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", "5.4.4")]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.0;

            Init();

            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left DoubleClick item 'MainForm.DataPanel.ColMenuItemRow25' at 30;13.", repo.MainForm.DataPanel.ColMenuItemRow25Info, new RecordItemIndex(0));
            repo.MainForm.DataPanel.ColMenuItemRow25.DoubleClick("30;13");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Delay", "Waiting for 10s.", new RecordItemIndex(1));
            Delay.Duration(10000, false);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MainForm.EnterServiceResultForm.TextBoxMaskBox4' at 15;7.", repo.MainForm.EnterServiceResultForm.TextBoxMaskBox4Info, new RecordItemIndex(2));
            repo.MainForm.EnterServiceResultForm.TextBoxMaskBox4.Click("15;7");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence from variable '$VarTSNumber_new' with focus on 'MainForm.EnterServiceResultForm.TextBoxMaskBox4'.", repo.MainForm.EnterServiceResultForm.TextBoxMaskBox4Info, new RecordItemIndex(3));
            repo.MainForm.EnterServiceResultForm.TextBoxMaskBox4.PressKeys(VarTSNumber_new);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MainForm.EnterServiceResultForm.Search1' at 41;13.", repo.MainForm.EnterServiceResultForm.Search1Info, new RecordItemIndex(4));
            repo.MainForm.EnterServiceResultForm.Search1.Click("41;13");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MainForm.EnterServiceResultForm.ServiceResultIMEIfield' at 32;7.", repo.MainForm.EnterServiceResultForm.ServiceResultIMEIfieldInfo, new RecordItemIndex(5));
            repo.MainForm.EnterServiceResultForm.ServiceResultIMEIfield.Click("32;7");
            Delay.Milliseconds(200);
            
            // Function for Random IMEI based on DateTime
            InputDifferentServiceResultIMEI();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MainForm.EnterServiceResultForm.BackCover' at 9;10.", repo.MainForm.EnterServiceResultForm.BackCoverInfo, new RecordItemIndex(7));
            repo.MainForm.EnterServiceResultForm.BackCover.Click("9;10");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MainForm.EnterServiceResultForm.TextBoxMaskBox' at 26;7.", repo.MainForm.EnterServiceResultForm.TextBoxMaskBoxInfo, new RecordItemIndex(8));
            repo.MainForm.EnterServiceResultForm.TextBoxMaskBox.Click("26;7");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence '10' with focus on 'MainForm.EnterServiceResultForm.TextBoxMaskBox'.", repo.MainForm.EnterServiceResultForm.TextBoxMaskBoxInfo, new RecordItemIndex(9));
            repo.MainForm.EnterServiceResultForm.TextBoxMaskBox.PressKeys("10");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MainForm.EnterServiceResultForm.Open' at 6;10.", repo.MainForm.EnterServiceResultForm.OpenInfo, new RecordItemIndex(10));
            repo.MainForm.EnterServiceResultForm.Open.Click("6;10");
            Delay.Milliseconds(200);
            
            Press_Down(ValueConverter.ArgumentFromString<int>("nr", ServiceResultCode));
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MainForm.EnterServiceResultForm.Save' at 33;11.", repo.MainForm.EnterServiceResultForm.SaveInfo, new RecordItemIndex(12));
            repo.MainForm.EnterServiceResultForm.Save.Click("33;11");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MainForm.EnterServiceResultForm.Close' at 30;13.", repo.MainForm.EnterServiceResultForm.CloseInfo, new RecordItemIndex(13));
            repo.MainForm.EnterServiceResultForm.Close.Click("30;13");
            Delay.Milliseconds(200);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
